package locationservices.android.eurecom.fr.locationservices;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by Federico on 01/12/2015.
 */
public class ProximityIntentReceiver extends BroadcastReceiver {

    String notificationTitle;
    String notificationContent;
    String tickerMessage;
    private int id;
    private static final String POINT_LATITUDE_KEY = "POINT_LATITUDE_KEY";
    private static final String POINT_LONGITUDE_KEY = "POINT_LONGITUDE_KEY";

    public ProximityIntentReceiver(int id) {
        this.id = id;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(getClass().getSimpleName(), "entered");
        Intent i = intent;
        String key = LocationManager.KEY_PROXIMITY_ENTERING;
        Boolean entering = intent.getBooleanExtra(key, false);
        Integer id = i.getIntExtra("id", -1);


        if (id!=-1){
            Log.i(getClass().getSimpleName(),id.toString());

        }else {
            Log.i(getClass().getSimpleName(), "id is null");
        }
        if(id==this.id) {
            if (entering) {

                Log.i(getClass().getSimpleName(), "entering");
                notificationTitle = "Proximity - Entry";
                notificationContent = "Entered the region";
                tickerMessage = "Entered the region";

            } else {

                Log.i(getClass().getSimpleName(), "exiting");
                notificationTitle = "Proximity - Exit";
                notificationContent = "Exited the region";
                tickerMessage = "Exited the region";

            }


            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Log.i(getClass().getSimpleName(), "exiting");
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(), 0);

            Notification notification = createNotification(context);
            notificationManager.notify((int) System.currentTimeMillis(), notification);
        }

    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private Notification createNotification(Context context) {
        Notification.Builder notificationBuilder = new Notification.Builder(context)
                .setWhen(System.currentTimeMillis())
                .setContentText(notificationContent)
                .setContentTitle(notificationTitle)
                .setSmallIcon(R.drawable.common_signin_btn_icon_dark)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS)
                .setTicker(tickerMessage);

        return notificationBuilder.build();
    }
}